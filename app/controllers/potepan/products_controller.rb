class Potepan::ProductsController < ApplicationController

  def index
    @taxonomy = Spree::Taxonomy.category_root
    @products = Spree::Variant.display_includes
                              .search_by_options(params[:color], params[:size])
                              .order_by(params[:sort])
    @options = Spree::OptionType.all
  end

  def show
    @product = Spree::Product.friendly.find(params[:slug_or_id])
    @related_products = Spree::Product.related_products(@product)
                                      .display_includes
                                      .take_random(4)
  end
end
