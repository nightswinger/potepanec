module ApplicationHelper
  # ページタイトルの設定
  def full_title(page_title='')
    base_title = 'BIGBAG Store'
    if page_title.empty?
      base_title
    else
      "#{page_title} - #{base_title}"
    end
  end

  def display_of_option_type_and_value
    option_values =
      Spree::OptionValue.where(presentation: [params[:color], params[:size]])
    if option_values.blank?
      'All PRODUCTS'
    else
      option_values.inject([]) do |array, option_value|
        array << "#{option_value.option_type.presentation} : #{option_value.presentation}"
      end.join(' ')
    end
  end
end
