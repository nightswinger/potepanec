Spree::Product.class_eval do
  scope :display_includes, -> { includes(master: [:images, :default_price]) }
  scope :order_by_recent,  -> { order(available_on: :desc) }
  scope :released,         -> { where('available_on < ?', Time.zone.now) }

  scope :related_products, lambda { |product|
    joins(:taxons).where(spree_taxons: { id: product.taxon_ids })
                  .where.not(id: product.id).distinct
  }

  def self.master_variants
    ids = all.pluck(:id)
    Spree::Variant.master_variants.where(product_id: ids)
  end

  def self.take_random(number)
    ids = all.pluck(:id).sample(number)
    where(id: ids)
  end
end
