require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe '#related_products' do
    let!(:category) { create(:taxonomy, name: 'category') }
    let!(:brand)    { create(:taxonomy, name: 'brand') }
    let!(:product)  { create(:product) }
    let!(:product_in_category) { create(:product) }
    let!(:product_in_brand)    { create(:product) }

    before do
      product.taxons << category.root
      product.taxons << brand.root
      product_in_category.taxons << category.root
      product_in_brand.taxons << brand.root
    end

    # 一つでも同じカテゴリーを持つ商品を返す
    it 'returns related product with one category' do
      expect(Spree::Product.related_products(product)).to include(product_in_category)
      expect(Spree::Product.related_products(product)).to include(product_in_brand)
    end

    # 関連商品には自身を含まない
    it 'dose not contain myself' do
      expect(Spree::Product.related_products(product)).not_to include(product)
    end
  end
end
