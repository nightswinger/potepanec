require 'rails_helper'

RSpec.describe Spree::Variant, type: :model do
  describe '#in_category' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:root)     { taxonomy.root }
    let!(:taxon)    { create(:taxon, name: 'books',  taxonomy: taxonomy, parent: root) }
    let!(:novels)   { create(:taxon, name: 'novels', taxonomy: taxonomy, parent: taxon) }
    let!(:comics)   { create(:taxon, name: 'comics', taxonomy: taxonomy, parent: taxon) }
    let!(:product1) { create(:product) }
    let!(:product2) { create(:product) }

    before do
      product1.taxons << novels
      product2.taxons << comics
    end

    it "returns variants in category" do
      expect(Spree::Variant.in_category(novels)).to include(product1.master)
      expect(Spree::Variant.in_category(novels)).to_not include(product2.master)
    end
  end

  describe '#order_by' do
    let!(:product1) { create(:product, price: 15.00, available_on: 1.week.ago) }
    let!(:product2) { create(:product, price: 20.00, available_on: Time.zone.now) }
    let!(:product3) { create(:product, price: 25.00, available_on: 1.days.ago)}

    context 'when a argument is empty' do
      it 'returns products in descending order by date' do
        expect(Spree::Variant.order_by.first).to eq product2.master
      end
    end

    context 'when a argument is correct' do
      it 'returns products ordered by price' do
        expect(Spree::Variant.order_by('priceDESC').first).to eq product3.master
        expect(Spree::Variant.order_by('priceASC').first).to  eq product1.master
      end

      it 'returns products ordered by date' do
        expect(Spree::Variant.order_by('dateDESC').first).to eq product2.master
        expect(Spree::Variant.order_by('dateASC').first).to  eq product1.master
      end
    end

    context 'when a argument is wrong' do
      it 'returns nil' do
        expect(Spree::Variant.order_by('fooBAR')).to be_nil
      end
    end
  end

  describe '#search_by_options' do
    let!(:color)       { create(:option_type, presentation: 'Color') }
    let!(:size)        { create(:option_type, presentation: 'Size') }
    let!(:red)         { create(:option_value, presentation: 'Red', option_type: color) }
    let!(:sizeS)       { create(:option_value, presentation: 'S', option_type: size) }
    let!(:sizeM)       { create(:option_value, presentation: 'M', option_type: size) }
    let!(:red_sizeS_variant) { create(:variant, option_values: [red, sizeS]) }
    let!(:red_sizeM_variant) { create(:variant, option_values: [red, sizeM]) }

    # 引数に一致するOptionValueが見つかるとき
    context 'when a option_value is found' do
      it 'returns appropriate variants' do
        expect(Spree::Variant.search_by_options('Red')).to     include(red_sizeS_variant)
        expect(Spree::Variant.search_by_options('Red')).to_not include(red_sizeS_variant.product.master)
      end
    end

    # 引数に一致するOptionValueが見つからないとき
    context 'when a option_value is not found' do
      it 'returns all master variants' do
        expect(Spree::Variant.search_by_options('foobar')).to     include(red_sizeS_variant.product.master)
        expect(Spree::Variant.search_by_options('foobar')).to_not include(red_sizeS_variant)
      end
    end

    # 引数の中身が空のとき
    context 'when a argument is empty' do
      it 'returns all master variants' do
        expect(Spree::Variant.search_by_options).to     include(red_sizeS_variant.product.master)
        expect(Spree::Variant.search_by_options).to_not include(red_sizeS_variant)
      end
    end

    context 'when there are multiple option_values' do
      it 'returns variants with both' do
        expect(Spree::Variant.search_by_options('Red', 'S')).to include(red_sizeS_variant)
        expect(Spree::Variant.search_by_options('Res', 'S')).to_not include(red_sizeM_variant)
      end
    end
  end
end
