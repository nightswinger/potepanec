Spree::Variant.class_eval do
  scope :master_variants,  -> { where(is_master: true) }
  scope :display_includes, -> { includes([product: { master: :default_price}], :images) }

  scope :order_by_dateASC,   -> { joins(:product).order('spree_products.available_on')}
  scope :order_by_dateDESC,  -> { joins(:product).order('spree_products.available_on desc')}
  scope :order_by_priceASC,  -> { joins(:default_price).order('spree_prices.amount')}
  scope :order_by_priceDESC, -> { joins(:default_price).order('spree_prices.amount desc')}

  def self.order_by(sort = 'dateDESC')
    sort = 'dateDESC' if sort.blank?
    try("order_by_#{sort}")
  end

  def self.in_category(taxon)
    joins(product: :taxons)
      .where(spree_taxons: { id: taxon.self_and_descendants.pluck(:id) })
      .master_variants
  end

  def self.search_by_options(*values)
    option_values = Spree::OptionValue.where(presentation: values)
    if option_values.blank?
      master_variants
    else
      option_values.inject(all) do |variants, option_value|
        variants.where(id: option_value.variants.pluck(:id))
      end
    end
  end
end
