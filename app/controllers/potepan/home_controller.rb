class Potepan::HomeController < ApplicationController
  DISPLAY_NUM = 6

  def index
    @new_products = Spree::Product.released
                                  .display_includes
                                  .order_by_recent.limit(DISPLAY_NUM)
  end
end
