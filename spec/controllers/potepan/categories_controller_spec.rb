require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do

  describe "GET #show" do
    let!(:categories) { create(:taxonomy, name: 'Categories') }
    let!(:bags)       { categories.root.children.create(name: 'Bags', taxonomy_id: categories.id) }

    it "returns a success response" do
      get :show, params: { id: bags.id }
      expect(response).to be_success
    end

    it "assigns the requested variable to @variables" do
      product1 = create(:product)
      product2 = create(:product)
      product3 = create(:product)
      bags.product_ids = product1.id, product2.id, product3.id

      get :show, params: { id: bags.id }
      expect(assigns(:taxonomy)).to eq categories.root
      expect(assigns(:taxon)).to eq bags
      expect(assigns(:products).count).to eq bags.products.count
    end
  end
end
