require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "get#show" do
    let!(:product) { create(:product, name: 'Example Jersey') }

    before do
      get :show, params: { slug_or_id: product.to_param }
    end

    it "returns a success response with a proper title" do
      expect(response).to be_success
    end

    it "assigns the requested product to @product" do
      expect(assigns(:product)).to eq product
    end
  end

  describe "GET#index" do
    let!(:categories)  { create(:taxonomy, name: 'Categories') }

    it "returns a success response" do
      get :index
      expect(response).to be_success
    end

    it "assigns the requested taxonomy to @taxonomy" do
      get :index
      expect(assigns(:taxonomy)).to eq categories.root
    end

    describe "@products" do
      let!(:color)       { create(:option_type, presentation: 'Color') }
      let!(:size)        { create(:option_type, presentation: 'Size') }
      let!(:red)         { create(:option_value, presentation: 'Red',  option_type: color) }
      let!(:blue)        { create(:option_value, presentation: 'Blue', option_type: color) }
      let!(:sizeS)       { create(:option_value, presentation: 'S', option_type: size) }
      let!(:red_variant) { create(:variant, option_values: [red]) }
      let!(:blue_variant){ create(:variant, option_values: [blue]) }
      let!(:blue_sizeS_v){ create(:variant, option_values: [blue, sizeS]) }
      let!(:option_type1){ create(:product_option_type, product: red_variant.product,  option_type: color) }
      let!(:option_type2){ create(:product_option_type, product: blue_variant.product, option_type: color) }

      context "when params is valid" do
        it "includes red variant" do
          get :index, params: { color: 'Red' }
          expect(assigns(:products)).to include(red_variant)
        end

        it "don't include blue variant" do
          get :index, params: { color: 'Red' }
          expect(assigns(:products)).to_not include(blue_variant)
        end
      end

      context "when params is nil" do
        it "returns master variants" do
          get :index
          expect(assigns(:products)).to_not include(red_variant)
          expect(assigns(:products)).to     include(red_variant.product.master)
        end
      end

      # OptionValueの絞り込み検索は可能だが
      # 現状ユーザーが画面のリンクから辿ることはできない
      context "when params have multiple option value params" do
        it "returns variants filtered by option values" do
          get :index, params: { color: 'Blue', size: 'S' }
          expect(assigns(:products)).to     include(blue_sizeS_v)
          expect(assigns(:products)).to_not include(blue_variant)
        end
      end
    end
  end
end
