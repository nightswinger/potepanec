Spree::OptionType.class_eval do
  scope :list_of_colors, -> { find_by(presentation: 'Color').option_values }
  scope :list_of_size,   -> { find_by(presentation: 'Size').option_values }
end
