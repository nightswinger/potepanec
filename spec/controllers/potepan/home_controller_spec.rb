require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

    describe "@new_products" do
      let!(:other_product)      { create(:product, available_on: 1.day.ago) }
      let!(:new_product)        { create(:product, available_on: Time.zone.now) }
      let!(:unreleased_product) { create(:product, available_on: 1.day.from_now) }

      it 'have new product at the beginning' do
        get :index
        expect(assigns(:new_products).first).to eq new_product
      end

      it 'do not include unreleased product' do
        get :index
        expect(assigns(:new_products)).not_to include(unreleased_product)
      end
    end

    describe "constant DISPLAY_NUM" do
      it 'is a number of new products to display' do
        expect(Potepan::HomeController::DISPLAY_NUM).to be_integer
      end
    end
  end
end
