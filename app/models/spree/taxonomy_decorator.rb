Spree::Taxonomy.class_eval do
  scope :category_root, -> { find_by(name: 'Categories').root }
end
