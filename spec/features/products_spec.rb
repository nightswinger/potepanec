require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: 'Categories') }
  given!(:color)    { create(:option_type, presentation: 'Color') }
  given!(:size)     { create(:option_type, presentation: 'Size') }
  given!(:red)      { create(:option_value, name: 'Red',   presentation: 'Red', option_type: color) }
  given!(:sizeS)    { create(:option_value, name: 'Small', presentation: 'S',   option_type: size) }

  scenario "pages have appropriate title" do
    product = create(:product, name: 'Example Jersey')

    visit potepan_product_path(product)
    expect(page).to have_title "Example Jersey - BIGBAG Store"
  end

  scenario "contents header have appropriate title" do
    create(:taxonomy, name: 'Categories')
    variant = create(:variant)

    variant.set_option_value('Color', 'Red')

    visit potepan_products_path
    expect(page).to have_content('All PRODUCTS')
    visit potepan_products_path(color: 'Red')
    expect(page).to have_content('Color : Red')
  end

  feature '#select box' do
    context 'when visit potepan_products_path' do
      scenario 'option "新着順" has selected attribute' do
        visit potepan_products_path
        expect(page).to have_select('sort', selected: '新着順')
      end
    end

    context 'when visit potepan_products_path with querystring' do
      background do
        visit potepan_products_path(sort: 'priceASC', color: 'Red')
      end

      scenario 'option that matches query has selected attribute' do
        expect(page).to have_select('sort', selected: '安い順')
      end

      scenario 'hidden field has option value query' do
        expect(find('input#color', visible: false)['value']).to eq 'Red'

        click_link 'Small'
        expect(find('input#size',  visible: false)['value']).to eq 'S'
      end
    end
  end

  feature '#filter by color and size sidebar' do
    scenario 'looking for various products' do
      visit potepan_products_path

      click_link 'Red'
      expect(page).to have_current_path(potepan_products_path(color: 'Red'))

      click_link 'Small'
      expect(page).to have_current_path(potepan_products_path(size: 'S'))

      click_link 'Red'
      expect(page).to have_current_path(potepan_products_path(color: 'Red'))
    end
  end
end
