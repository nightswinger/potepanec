class Potepan::CategoriesController < ApplicationController

  def show
    @taxonomy = Spree::Taxonomy.category_root
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Variant.in_category(@taxon)
                              .display_includes
                              .order_by(params[:sort])
    @options = Spree::OptionType.all
  end
end
